/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at
 * trunk/opends/resource/legal-notices/OpenDS.LICENSE
 * or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at
 * trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 * add the following below this CDDL HEADER, with the fields enclosed
 * by brackets "[]" replaced with your own identifying information:
 *      Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 *
 *
 *       Copyright 2011 Sun Microsystems, Inc.
 */

package org.opends.server.types;

import org.opends.messages.Message;

/**
 * This interface defines a set of methods that may be used to notify
 * various Directory Server components whenever an operation is
 * requested to cancel.  Note that the operation is may not be
 * cancelled when the listener is invoked.
 */
public interface CancelRequestListener
{
  /**
   * Performs any processing that might be necessary to cancel
   * the operation.
   *
   * @param  cancelReason A message that explains the purpose for
   *                      this cancellation.
   */
  void handleOperationCancelRequest(Message cancelReason);
}
