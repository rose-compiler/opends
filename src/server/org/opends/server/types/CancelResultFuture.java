/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at
 * trunk/opends/resource/legal-notices/OpenDS.LICENSE
 * or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at
 * trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 * add the following below this CDDL HEADER, with the fields enclosed
 * by brackets "[]" replaced with your own identifying information:
 *      Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 *
 *
 *       Copyright 2011 Sun Microsystems, Inc.
 */
package org.opends.server.types;

import org.opends.messages.Message;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * Defines the set of possible outcomes that can result from
 * processing a cancel request asynchronously.
 */
@org.opends.server.types.PublicAPI(
     stability=org.opends.server.types.StabilityLevel.UNCOMMITTED,
     mayInstantiate=false,
     mayExtend=false,
     mayInvoke=true)
public interface CancelResultFuture extends Future<ResultCode>
{
  /**
   * Retrieves the result code associated with this cancel result,
   * waiting if necessary.
   *
   * @return  The result code associated with this cancel result.
   * @throws InterruptedException If the current thread was
   *                              interrupted while waiting.
   */
  public ResultCode get() throws InterruptedException;

  /**
   * Retrieves the result code associated with this cancel result,
   * waiting if necessary for at most the given time.
   *
   * @param l the maximum time to wait,
   * @param timeUnit the time unit of the timeout argument.
   * @return The result code associated with this cancel result.
   * @throws InterruptedException If the current thread was
   *                              interrupted while waiting.
   * @throws ExecutionException if the computation threw an exception.
   * @throws TimeoutException If the wait timed out.
   */
  public ResultCode get(long l, TimeUnit timeUnit)
      throws InterruptedException, ExecutionException,
      TimeoutException;

  /**
   * Retrieves the human-readable response that the server provided
   * for the result of the cancellation, waiting if necessary.
   *
   * @return  The buffer that is used to hold a human-readable
   *          response that the server provided for the result of this
   *          cancellation.
   * @throws InterruptedException If the current thread was
   *                              interrupted while waiting.
   */
  public Message getResponseMessage() throws InterruptedException;

  /**
   * Cancel requests can not be cancelled. This method will always
   * throw a {@code UnsupportedOperationException}.
   *
   * @param b {@code true} if the thread executing this task should be
   *                       interrupted; otherwise, in-progress tasks
   *                       are allowed to complete.
   * @return false if the task could not be cancelled, typically
   *         because it has already completed normally; true
   *         otherwise.
   */
  boolean cancel(boolean b);

  /**
   * Since cancel requests can not be cancelled, this will always
   * return false.
   *
   * @return {@code false}.
   */
  boolean isCancelled();
}

