/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the license at
 * trunk/opends/resource/legal-notices/OpenDS.LICENSE
 * or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at
 * trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 * add the following below this CDDL HEADER, with the fields enclosed
 * by brackets "[]" replaced with your own identifying information:
 *      Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 *
 *
 *       Copyright 2011 Sun Microsystems, Inc.
 */
package org.opends.server.types;
import org.opends.messages.Message;

import java.util.concurrent.*;

/**
 * This class defines a data structure that can be used to hold
 * information about a request to cancel or abandon an operation in
 * progress.
 */
@org.opends.server.types.PublicAPI(
     stability=org.opends.server.types.StabilityLevel.UNCOMMITTED,
     mayInstantiate=false,
     mayExtend=false,
     mayInvoke=true)
public final class CancelRequest implements CancelResultFuture
{
  // Indicates whether to send a response to the original request if
  // the operation is canceled.
  private final boolean notifyOriginalRequestor;

  // A message that explains the purpose for this cancellation (may be
  // included in the response to the original requestor).
  private final Message cancelReason;

  private final CancelResultHandler resultHandler;

  private final CountDownLatch sync = new CountDownLatch(1);

    // The result code associated with this cancel result.
  private volatile ResultCode resultCode;

  // A human-readable response that the server
  // provided for the result of the cancellation.
  private volatile Message responseMessage;

  /**
   * Creates a new cancel request with the provided information.
   *
   * @param  notifyOriginalRequestor  Indicates whether the original
   *                                  requestor should receive a
   *                                  response if the operation is
   *                                  canceled.
   * @param  cancelReason             A message that explains the
   *                                  purpose for this cancellation.
   * @param resultHandler             The result handler to notify
   *                                  once the cancel request is
   *                                  completed.
   */
  public CancelRequest(boolean notifyOriginalRequestor,
                       Message cancelReason,
                       CancelResultHandler resultHandler)
  {
    this.notifyOriginalRequestor = notifyOriginalRequestor;
    this.cancelReason            = cancelReason;
    this.resultHandler           = resultHandler;
  }



  /**
   * Indicates whether the original requestor should receive a
   * response to the request if the operation is canceled.
   *
   * @return  <CODE>true</CODE> if the original requestor should
   *          receive a response if the operation is canceled, or
   *          <CODE>false</CODE> if not.
   */
  public boolean notifyOriginalRequestor()
  {
    return notifyOriginalRequestor;
  }



  /**
   * Retrieves a message that explains the purpose for this
   * cancellation.
   *
   * @return  A message that explains the purpose for this
   *          cancellation.
   */
  public Message getCancelReason()
  {
    return cancelReason;
  }

  /**
   * {@inheritDoc}
   */
  public ResultCode get() throws InterruptedException
  {
    sync.await();
    return this.resultCode;
  }

  /**
   * {@inheritDoc}
   */
  public boolean cancel(boolean b) {
    throw new UnsupportedOperationException();
  }

  /**
   * {@inheritDoc}
   */
  public boolean isCancelled() {
    return false;
  }

  /**
   * {@inheritDoc}
   */
  public boolean isDone() {
    return sync.getCount() == 0;
  }

  /**
   * {@inheritDoc}
   */
  public ResultCode get(long l, TimeUnit timeUnit)
      throws InterruptedException, ExecutionException,
      TimeoutException
  {
    sync.await(l, timeUnit);
    return this.resultCode;
  }

  /**
   * {@inheritDoc}
   */
  public Message getResponseMessage() throws InterruptedException
  {
    sync.await();
    return this.responseMessage;
  }

  /**
   * Set the results of this cancel request. The result handler
   * and/or waiting threads (if any) will be notified of the
   * results.
   *
   * @param resultCode The result code of the cancel request.
   * @param responseMessage The response for the result of the
   *        cancellation.
   */
  public void setResult(ResultCode resultCode,
                        Message responseMessage)
  {
    this.resultCode = resultCode;
    this.responseMessage = responseMessage;
    sync.countDown();

    if(resultHandler != null)
    {
      resultHandler.handleCancelResult(resultCode, responseMessage);
    }
  }

  /**
   * Creates a completed CancelResultFuture instance with the
   * specified results.
   *
   * @param resultCode The result code of the cancel request.
   * @param responseMessage The response for the result of the
   *                        cancellation.
   * @param resultHandler The result handler to notify once the
   *                      cancel request is completed.
   * @return The completed CancelResultFuture instance.
   */
  public static CancelResultFuture completedResult(
      final ResultCode resultCode, final Message responseMessage,
      final CancelResultHandler resultHandler)
  {
    if(resultHandler != null)
    {
      resultHandler.handleCancelResult(resultCode, responseMessage);
    }

    return new CancelResultFuture()
    {
      public ResultCode get() throws InterruptedException {
        return resultCode;
      }

      public ResultCode get(long l, TimeUnit timeUnit)
          throws InterruptedException, ExecutionException,
          TimeoutException
      {
        return resultCode;
      }

      public Message getResponseMessage()
          throws InterruptedException
      {
        return responseMessage;
      }

      public boolean cancel(boolean b) {
        throw new UnsupportedOperationException();
      }

      public boolean isCancelled() {
        return false;
      }

      public boolean isDone() {
        return true;
      }
    };
  }
}

