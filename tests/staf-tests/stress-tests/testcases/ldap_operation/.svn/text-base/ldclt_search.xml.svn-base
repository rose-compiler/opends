<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE stax SYSTEM "../../../shared/stax.dtd">
<!--
 ! CDDL HEADER START
 !
 ! The contents of this file are subject to the terms of the
 ! Common Development and Distribution License, Version 1.0 only
 ! (the "License").  You may not use this file except in compliance
 ! with the License.
 !
 ! You can obtain a copy of the license at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE
 ! or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 ! See the License for the specific language governing permissions
 ! and limitations under the License.
 !
 ! When distributing Covered Code, include this CDDL HEADER in each
 ! file and include the License file at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 ! add the following below this CDDL HEADER, with the fields enclosed
 ! by brackets "[]" replaced with your own identifying information:
 !      Portions Copyright [yyyy] [name of copyright owner]
 !
 ! CDDL HEADER END
 !
 !      Copyright 2009-2010 Sun Microsystems, Inc.
 ! -->
<stax>

  <defaultcall function="ldclt_search"/>

  <function name="ldclt_search">

    <sequence>

      <block name="'ldclt-search'">
      
        <sequence>
  
          <script>
            if not CurrentTestPath.has_key('group'):
              CurrentTestPath['group']='ldap_operation'
            CurrentTestPath['suite']=STAXCurrentBlock
          </script>
      
          <call function="'testSuite_Preamble'"/>

          <script>
            searchLoop      = testsDuration / 10
            lowRange        = 100000
            highRange       = lowRange + numberOfEntries
            jvmLoop         = testsDuration / 1800 + 1
            jvmSleep        = 1800000
            baseDN          = 'ou=People,dc=com'
            ldcltDir        = '%s/ldclt_dir' % TMPDIR
            ldifTemplate    = '%s/%s_entries.template' % (remote.temp, numberOfEntries)
            ldifFile        = '%s/%s_entries.ldif' % (remote.temp, numberOfEntries)
            totalEntries    = numberOfEntries + 2
          </script>

          <import machine="STAF_LOCAL_HOSTNAME"
            file="'%s/testcases/ldap_operation/ldap_operation_setup.xml' % TESTS_DIR"/>
          <call function="'ldap_operation_setup'">
            {
            'startDS'     : False ,
            'loadBackend' : False
            }
          </call>

          <testcase name="getTestCaseName('Preamble')">
            
            <sequence>

              <call function="'testCase_Preamble'"/>

              <!-- Check if 'ldifFile' exists -->
              <call function="'GetEntry'">
                {
                'location'  : STAF_REMOTE_HOSTNAME ,
                'entry'     : ldifFile ,
                'attribute' : 'TYPE'
                }
              </call>
              <if expr="RC == 48">
                <sequence>
                  <message>
                    'ldclt_search: File %s does not exist, create it' % ldifFile
                  </message>
                  
                  <message>
                    'ldclt_search: Make an make-ldif template file'
                  </message>
                  
                  <call function="'MakeAMakeLdifTemplate'">
                    {
                    'numEntries'           : numberOfEntries ,
                    'startFrom'            : lowRange ,
                    'additionalAttributes' : True ,
                    'templateFile'         : ldifTemplate ,
                    'templateLocation'     : STAF_REMOTE_HOSTNAME
                    }
                  </call>

                  <message>
                    'ldclt_search: Make the ldif file'
                  </message>

                  <call function="'MakeLdifWithScript'">
                    {
                    'location'     : STAF_REMOTE_HOSTNAME ,
                    'templateFile' : ldifTemplate ,
                    'ldifFile'     : ldifFile
                    }
                  </call>
                </sequence>
                <else>
                  <message>
                    'ldclt_search: File %s exists, do nothing' % ldifFile
                  </message>
                </else>
              </if>

              <message>
               'ldclt_search: Import large ldif file'
              </message>

              <call function="'ImportLdifWithScript'">
                {
                'location'   : STAF_REMOTE_HOSTNAME ,
                'ldifFile'   : ldifFile ,
                'backEnd'    : 'userRoot'
                }
              </call>

              <script>
               returnString = STAXResult[0][1]
              </script>

              <call function="'checktestString'">
                {
                'returnString'   : returnString ,
                'expectedString' : 'Processed %s entries' % totalEntries
                }
              </call>

              <message>
                'ldclt_search: Start DS running on port %s' \
                % (DIRECTORY_INSTANCE_PORT)
              </message>

              <call function="'StartDsWithScript'">
                { 'location' : STAF_REMOTE_HOSTNAME }
              </call>

              <!--- Check that DS started -->
              <call function="'isAlive'">
                {
                'noOfLoops'        : 10 ,
                'noOfMilliSeconds' : 5000
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>


          <testcase name="getTestCaseName('Run ldclt search')">
            
            <sequence>
              
              <call function="'testCase_Preamble'"/>

              <message>
                 'ldclt_search: Run ldclt search'
              </message>

              <!--<timer duration="timerDuration">
                <sequence>-->
                  <parallel>
                    <block name="'ldclt-search-threads'">
                    <sequence>
                      
                      <message>
                        'ldclt_search: search threads'
                      </message>

                      <script>
                        opt1 = '-e delayedstartup=1 -r %s -R %s' \
                                % (lowRange, highRange)
                        opt2 = '-e esearch,random -f uid=user.XXXXXX'
                        opt3 = '-N %s -I 32 -v -q' % searchLoop
                        opts = '%s %s %s' % (opt1, opt2, opt3)
                      </script>

                      <call function="'ldclt'">
                      {
                      'location'       : STAF_CLIENT_HOSTNAME ,
                      'dsInstanceHost' : DIRECTORY_INSTANCE_HOST,
                      'dsInstanceDn'   : DIRECTORY_INSTANCE_DN ,
                      'dsInstancePswd' : DIRECTORY_INSTANCE_PSWD ,
                      'dsBaseDn'       : baseDN ,
                      'ldcltOptions'   : opts ,
                      'outputFile'     : 'ldclt_search.res' ,
                      'outputPath'     : ldcltDir
                      }
                      </call>
                      
                    </sequence>
                    </block>
                    <block name="'ldclt-jvm'">
                    <sequence>
                      <loop from="1" to="jvmLoop" var="loop">
                        <sequence>
                          <message>'LOOP %s on %s' % (loop, jvmLoop)</message>
                          
                          <message>
                            'ldclt_search: save jvm information'
                          </message>

                          <call function="'ldapSearchWithScript'">
                            {
                            'location'       : STAF_REMOTE_HOSTNAME ,
                            'dsInstanceHost' : DIRECTORY_INSTANCE_HOST ,
                            'dsInstancePort' : DIRECTORY_INSTANCE_PORT ,
                            'dsInstanceDn'   : DIRECTORY_INSTANCE_DN ,
                            'dsInstancePswd' : DIRECTORY_INSTANCE_PSWD ,
                            'dsBaseDN'       : 'cn=JVM Memory Usage,cn=monitor',
                            'dsFilter'       : 'objectclass=*' ,
                            'expectedRC'     : 'noCheck' ,
                            'outputFile'     : 'jvm_search_%s.out' % loop ,
                            'outputPath'     : remote.temp
                            }
                          </call>

                          <if expr="loop != jvmLoop">
                            <call function="'Sleep'">
                              { 'sleepForMilliSeconds'  : jvmSleep }
                            </call>
                          </if>
                        </sequence>
                      </loop>
                    </sequence>
                    </block>
                  </parallel>
              <!--</sequence>
              </timer>

              <script>timerRC = RC</script>

              <if expr="timerRC == 0">
                <sequence>
                  <message>'Timer exceeds %s' % timerDuration</message>
                  <tcstatus result="'fail'"/>
                </sequence>
              </if>-->

              <message>
                'Copy %s/ldclt_search.res from %s to %s on %s' \
                % (ldcltDir,STAF_CLIENT_HOSTNAME,logs.sut,STAXServiceMachine)
              </message>
              <call function="'copyFile'">
                {
                'location'   : STAF_CLIENT_HOSTNAME ,
                'srcfile'    : '%s/ldclt_search.res' % ldcltDir ,
                'destfile'   : '%s/ldclt_search.res' % logs.sut ,
                'remotehost' : STAXServiceMachine
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>
            
          </testcase>


          <testcase name="getTestCaseName('Check server is still running')">

            <sequence>
              
              <call function="'testCase_Preamble'"/>

              <message>
                'ldclt_search: Check server is still running on port %s' \
                % (DIRECTORY_INSTANCE_PORT)
              </message>

              <!--- Check that DS started -->
              <call function="'isAlive'">
                {
                'noOfLoops'        : 10 ,
                'noOfMilliSeconds' : 5000
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>
          
          <import machine="STAF_LOCAL_HOSTNAME"
            file="'%s/testcases/ldap_operation/ldap_operation_cleanup.xml' % (TESTS_DIR)"/>
          <call function="'ldap_operation_cleanup'">
            { 'suiteSuffix' : 'search' }
          </call>

          <call function="'testSuite_Postamble'"/>
            
        </sequence>
       
      </block>
       
    </sequence>
      
  </function>

</stax>
