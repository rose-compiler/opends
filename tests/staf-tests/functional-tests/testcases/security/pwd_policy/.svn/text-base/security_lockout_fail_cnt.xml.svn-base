<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE stax SYSTEM "../../../../shared/stax.dtd">
<!--
 ! CDDL HEADER START
 !
 ! The contents of this file are subject to the terms of the
 ! Common Development and Distribution License, Version 1.0 only
 ! (the "License").  You may not use this file except in compliance
 ! with the License.
 !
 ! You can obtain a copy of the license at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE
 ! or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 ! See the License for the specific language governing permissions
 ! and limitations under the License.
 !
 ! When distributing Covered Code, include this CDDL HEADER in each
 ! file and include the License file at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 ! add the following below this CDDL HEADER, with the fields enclosed
 ! by brackets "[]" replaced with your own identifying information:
 !      Portions Copyright [yyyy] [name of copyright owner]
 !
 ! CDDL HEADER END
 !
 !      Copyright 2006-2008 Sun Microsystems, Inc.
 ! -->
<stax>

  <defaultcall function="lockout_fail_cnt"/>

  <function name="lockout_fail_cnt">

      <sequence>
        
          <!--- Test Suite information
            #@TestSuiteName       Lockout Failure Count
            #@TestSuitePurpose    Test the password Policy Lockout Failure Count
            #@TestSuiteGroup      Lockout Failure Count
            #@TestScript          security_lockout_fail_cnt.xml
          -->
          
   <!--- Define default value for basedn,basepwp, msg -->
   <script>
   basedn = 'ou=people,ou=password tests,o=Pwd Policy Tests,dc=example,dc=com'
   basepwp = 'cn=Default Password Policy,cn=Password Policies,cn=config'
   msg = 'Security: Lockout Fail Cnt:'
   msg1 = 'get-remaining-authentication-failure-count'
   msg2 = 'Remaining Authentication Failure Count:'
   </script>

        <!--- Test Case information
          #@TestMarker          Lockout Failure Count
          #@TestName            Test Preamble
          #@TestIssue           none
          #@TestPurpose         Check Default value ds-cfg-lockout-failure-count
          #@TestPreamble        none
          #@TestStep            Step 1. Check for existence of
                                ds-cfg-lockout-failure-count and value is 0
          #@TestStep            Step 2. Default Bind With Bad Pwd 3x RC 49
                                then User search with good password returns 0
          #@TestStep            Step 3. check manage-account
                                get-remaining-authentication-failure-count
          #@TestPostamble       none
          #@TestResult          Success if the 3 steps are PASS
        -->
          
        <testcase name="getTestCaseName('Preamble')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
'%s Preamble Step 1. Check for existence of ds-cfg-lockout-failure-count' % msg
            </message>

            <call function="'compareEntry'">
              { 'dsInstanceHost'      : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'      : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'        : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'      : DIRECTORY_INSTANCE_PSWD ,
                'attrToBeCompared'    : 'ds-cfg-lockout-failure-count:0',
                'entryToBeCompared'   : basepwp }
            </call>

            <message>
               '%s Preamble Step 2. Default Bind With Bad Pwd 3x' % msg
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=bhall,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' ,
                  'expectedRC'       : 49 }
              </call>
            </iterate>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=bhall,%s' % basedn ,
                  'dsInstancePswd'   : 'oranges',
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' }
            </call>
            
            <message>
               '%s Preamble Step 3. Check manage-account %s' % (msg,msg1)
            </message>

            <call function="'manageAccountWithScript'">
               { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                 'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                 'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                 'subcommand'       : msg1 ,
                 'targetDn'         : 'uid=bhall,%s' % basedn  }
            </call>
          
            <script> 
               returnString = STAXResult[0][1]
            </script>
            
            <call function="'checktestString'">
               { 'returnString'       : returnString ,
                 'expectedString'     : msg2 }
            </call>
            
            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

        <!--- Test Case information
          #@TestMarker          Lockout Failure Count
          #@TestName            Admin Change Lockout Count
          #@TestIssue           none
          #@TestPurpose         Admin Change Lockout Count
          #@TestPreamble        none
          #@TestStep            Admin Changing Lockout Count set
                                lockout-failure-count to 3
          #@TestStep            check manage-account
                                get-remaining-authentication-failure-count
          #@TestPostamble       none
          #@TestResult          Success if all tests are PASS
        -->

        <testcase name="getTestCaseName('Admin Change Lockout Cnt')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Lockout Fail Cnt: Admin Changing Lockout Count'
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-failure-count' ,
                    'attributeValue'         : '3' }
            </call>
            
            <message>
               '%s Check manage-account %s' % (msg,msg1)
            </message>

            <call function="'manageAccountWithScript'">
               { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                 'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                 'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                 'subcommand'       : msg1 ,
                 'targetDn'         : 'uid=bhall,%s' % basedn  }
            </call>
          
            <script> 
               returnString = STAXResult[0][1]
            </script>
            
            <call function="'checktestString'">
               { 'returnString'       : returnString ,
                 'expectedString'     : '%s  3' % msg2 }
            </call>
            
            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
          #@TestMarker          Lockout Failure Count
          #@TestName            Lockout With Bad Pwd 3x
          #@TestIssue           none
          #@TestPurpose         Lockout With Bad Pwd 3x
          #@TestPreamble        none
          #@TestStep            Lockout With Bad Pwd 3x SearchObject RC 49 3x
          #@TestStep            check manage-account
                                get-remaining-authentication-failure-count
          #@TestPostamble       none
          #@TestResult          Success if all tests are PASS
        -->
        
        <testcase name="getTestCaseName('Lockout With Bad Pwd 3x')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Lockout With Bad Pwd 3x' % msg
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad', 'oranges']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=bhall,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' ,
                  'expectedRC'       : 49 }
              </call>
            </iterate>
            
            <message>
               '%s Check manage-account %s' % (msg,msg1)
            </message>

            <call function="'manageAccountWithScript'">
               { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                 'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                 'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                 'subcommand'       : msg1 ,
                 'targetDn'         : 'uid=bhall,%s' % basedn  }
            </call>
          
            <script> 
               returnString = STAXResult[0][1]
            </script>
            
            <call function="'checktestString'">
               { 'returnString'       : returnString ,
                 'expectedString'     : '%s  0' % msg2 }
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

        <!--- Test Case information
          #@TestMarker        Lockout Failure Count
          #@TestName          Postamble Reset
          #@TestIssue         none
          #@TestPurpose       Reseting the Password policy and verify it
          #@TestPreamble      none
          #@TestStep          Step 1. Admin Resetting Lockout Fail Count
                              set lockout-failure-count to 0
          #@TestStep          Step 2. Check Bind With Previous User Lockout RC 0
          #@TestStep          Step 3. Check Bind With Bad Pwd 3x RC 49 3x
                              then User search with good password returns 0
          #@TestPostamble     none
          #@TestResult        Success if the 3 steps are PASS
        -->
        
        <testcase name="getTestCaseName('Postamble Reset')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
'%s Postamble Reset: Step 1. Admin Resetting Lockout Fail Count' % msg
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-failure-count' ,
                    'attributeValue'         : '0' }
            </call>
            
            <message>
'%s Postamble Step 2. Check Bind With Previous User Lockout' % msg
            </message>

            <!--- Check Lockouted User -->
           <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=bhall,%s' % basedn ,
                'dsInstancePswd'   : 'oranges' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*'  ,
                'extraParams'      : '-s base' }
            </call>

            <message>
      '%s Postamble Step 3. Check Bind With Bad Pwd 3x' % msg
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=gfarmer,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base', 
                  'expectedRC'       : 49 } 
              </call>
            </iterate>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=gfarmer,%s' % basedn ,
                  'dsInstancePswd'   : 'ruling',
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base'}
            </call>

            <call function="'testCase_Postamble'"/>

           </sequence>
        </testcase>

     </sequence>

  </function>

</stax>
