<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE stax SYSTEM "../../../../shared/stax.dtd">
<!--
 ! CDDL HEADER START
 !
 ! The contents of this file are subject to the terms of the
 ! Common Development and Distribution License, Version 1.0 only
 ! (the "License").  You may not use this file except in compliance
 ! with the License.
 !
 ! You can obtain a copy of the license at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE
 ! or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 ! See the License for the specific language governing permissions
 ! and limitations under the License.
 !
 ! When distributing Covered Code, include this CDDL HEADER in each
 ! file and include the License file at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 ! add the following below this CDDL HEADER, with the fields enclosed
 ! by brackets "[]" replaced with your own identifying information:
 !      Portions Copyright [yyyy] [name of copyright owner]
 !
 ! CDDL HEADER END
 !
 !      Copyright 2006-2008 Sun Microsystems, Inc.
 ! -->
<stax>

  <defaultcall function="preencoded_pwds"/>

  <function name="preencoded_pwds">

      <sequence>

         <!--- Test Suite information
           #@TestSuiteName       Preencoded Passwords
           #@TestSuitePurpose    Test Preencoded Passwords in Password Policy
           #@TestSuiteGroup      Preencoded Passwords
           #@TestScript          security_preencoded_pwds.xml
          -->
                 
     <!--- Define default value for basedn -->
     <script>
     basedn = 'ou=people,ou=password tests,o=Pwd Policy Tests,dc=example,dc=com'
     basepwp = 'cn=Default Password Policy,cn=Password Policies,cn=config'
     msg1 = 'Security: Preencoded Pwds: Preamble'
     </script>
     
     
        <!--- Test Case information
            #@TestMarker          Preencoded Passwords
            #@TestName            Preamble Check Default
            #@TestIssue           none
            #@TestPurpose         Preamble Check Default
            #@TestPreamble        none
            #@TestStep            Check for existence of 
                                  ds-cfg-allow-pre-encoded-passwords
                                  and value is set to false
            #@TestPostamble       none
            #@TestResult          Success if the test is PASS
        -->
        
        <testcase name="getTestCaseName('Preamble')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
'%s - Check for existence of ds-cfg-allow-pre-encoded-passwords' % msg1
            </message>

            <call function="'compareEntry'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'attrToBeCompared' : 'ds-cfg-allow-pre-encoded-passwords:false',
                'entryToBeCompared': basepwp }
            </call>

            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
            #@TestMarker          Preencoded Passwords
            #@TestName            Add Entry With Pre-encoded Password
            #@TestIssue           none
            #@TestPurpose         Adding New Entry With a Pre-encoded Password
            #@TestPreamble        none
            #@TestStep            addEntry add_entry_preencoded_pwd.ldif RC 53
                                  SearchObject returns 49
            #@TestPostamble       none
            #@TestResult          Success if the test is PASS
        -->
        
        <testcase name="getTestCaseName('Default - Add New Entry')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
      'Security: Preencoded Pwds: Adding New Entry With a Pre-encoded Password'
            </message>

   <call function="'addEntry'">
 { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
   'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
   'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
   'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
   'entryToBeAdded'   : '%s/security/pwd_policy/add_entry_preencoded_pwd.ldif' \
                        % remote.data ,
   'expectedRC'       : 53 }
   </call>

             <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=pguy,%s' % basedn ,
                'dsInstancePswd'   : 'superguy' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*'  ,
                'extraParams'      : '-s base' ,
                'expectedRC'       : 49 }
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

        <!--- Test Case information
            #@TestMarker          Preencoded Passwords
            #@TestName            Enable Pre-encoded Passwords in pwd policy
            #@TestIssue           none
            #@TestPurpose         Admin Enabling Preencoded Pwds
            #@TestPreamble        none
            #@TestStep            set allow-pre-encoded-passwords to true
            #@TestPostamble       none
            #@TestResult          Success if the test is PASS
        -->
        
        <testcase name="getTestCaseName('Enable Preencoded Pwds')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Preencoded Pwds: Admin Enabling Preencoded Pwds'
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'allow-pre-encoded-passwords' ,
                    'attributeValue'         : 'true' }
            </call>
            
            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

         <!--- Test Case information
            #@TestMarker          Preencoded Passwords
            #@TestName            Test Enabled Pre-encoded Passwords
            #@TestIssue           none
            #@TestPurpose         Test Enabled Pre-encoded Passwords
            #@TestPreamble        none
            #@TestStep            addEntry add_entry_preencoded_pwd.ldif RC 0
                                  SearchObject returns 0
            #@TestPostamble       none
            #@TestResult          Success if the test is PASS
        -->           

        <testcase name="getTestCaseName('Enabled - Add New Entry')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
           'Security: Preencoded Pwds: Add New Entry With Pre-encoded Password'
            </message>

   <call function="'addEntry'">
 { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
   'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
   'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
   'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
   'entryToBeAdded'   : '%s/security/pwd_policy/add_entry_preencoded_pwd.ldif' \
                        % remote.data }
   </call>

             <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=pguy,%s' % basedn ,
                'dsInstancePswd'   : 'superguy' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*'  ,
                'extraParams'      : '-s base' }
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

         <!--- Test Case information
            #@TestMarker          Preencoded Passwords
            #@TestName            Disable Pre-encoded Passwords
            #@TestIssue           none
            #@TestPurpose         Admin Disabling Pre-encoded Passwords
            #@TestPreamble        none
            #@TestStep            Disable Pre-encoded Passwordds 0
                                  SearchObject returns 49
            #@TestPostamble       none
            #@TestResult          Success if the test is PASS
        -->
        
        <testcase name="getTestCaseName('Disable Preencoded Pwds')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Preencoded Pwds: Admin Disabling Pre-encoded Pwds'
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'allow-pre-encoded-passwords' ,
                    'attributeValue'         : 'false' }
            </call>
            
            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=pgal,%s' % basedn ,
                'dsInstancePswd'   : 'supergal' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*'  ,
                'extraParams'      : '-s base' ,
                'expectedRC'       : 49 }
            </call>

            <call function="'testCase_Postamble'"/>

         </sequence>
        </testcase>

     </sequence>

  </function>

</stax>
