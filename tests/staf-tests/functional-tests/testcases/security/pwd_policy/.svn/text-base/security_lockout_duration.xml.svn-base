<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE stax SYSTEM "../../../../shared/stax.dtd">
<!--
 ! CDDL HEADER START
 !
 ! The contents of this file are subject to the terms of the
 ! Common Development and Distribution License, Version 1.0 only
 ! (the "License").  You may not use this file except in compliance
 ! with the License.
 !
 ! You can obtain a copy of the license at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE
 ! or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 ! See the License for the specific language governing permissions
 ! and limitations under the License.
 !
 ! When distributing Covered Code, include this CDDL HEADER in each
 ! file and include the License file at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 ! add the following below this CDDL HEADER, with the fields enclosed
 ! by brackets "[]" replaced with your own identifying information:
 !      Portions Copyright [yyyy] [name of copyright owner]
 !
 ! CDDL HEADER END
 !
 !      Copyright 2006-2010 Sun Microsystems, Inc.
 ! -->
<stax>

  <defaultcall function="lockout_duration"/>

  <function name="lockout_duration">

      <sequence>
        
         <!--- Test Suite information
           #@TestSuiteName       Lockout Duration
           #@TestSuitePurpose    Test Lockout Duration in Password Policy
           #@TestSuiteGroup      Lockout Duration
           #@TestScript          security_lockout_duration.xml
          -->
 
	<!--- Define default value for basedn -->
          <script>
            basedn1 = 'ou=people,ou=password tests,'
            basedn = '%s o=Pwd Policy Tests,dc=example,dc=com' % basedn1
            pwp = 'cn=Default Password Policy,cn=Password Policies,cn=config'
            msg = 'Security: Lockout Duration:'
            msg1 = 'Security: Lockout Duration: Preamble'
            msg2 = 'Security: Lockout Duration: Short Lockout Duration'
            msg3 = 'get-seconds-until-password-reset-lockout'
            msg4 = 'Security: Lockout Duration: Long Lockout Duration'
          </script>

         
        <!--- Test Case information
           #@TestMarker         Lockout Duration
           #@TestName           Preamble
           #@TestIssue          none
           #@TestPurpose        Preamble
           #@TestPreamble       none
           #@TestStep           Step 1. Checking existence of 
                                ds-cfg-lockout-duration compareEntry returns 0
           #@TestStep           Step 2. Admin Changing Lockout Count
                                set lockout-failure-count to 3 
           #@TestStep           Step 3. User Lockout With Bad Pwd 3x 
                                SearchObject returns 49 (3x with bad, 
                                1x with correct one)
           #@TestStep           Step 4. Admin Resetting User Pwd
           #@TestStep           Step 5. User Bind With New Password
                                SearchObject returns 0
           #@TestPostamble      none
           #@TestResult         Success if the 5 steps are PASS
        -->
        
        
        <testcase name="getTestCaseName('Preamble')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Step 1. Checking existence of ds-cfg-lockout-duration' % msg1
            </message>

            <call function="'compareEntry'">
              { 'dsInstanceHost'      : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'      : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'        : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'      : DIRECTORY_INSTANCE_PSWD ,
                'attrToBeCompared'    : 'ds-cfg-lockout-duration:0 seconds',
                'entryToBeCompared'   : pwp }
            </call>

            <message>
               '%s Step 2. Admin Changing Lockout Count' % msg1
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-failure-count' ,
                    'attributeValue'         : '3' }
            </call>
            
            <message>
               '%s Step 3. User Lockout With Bad Pwd 3x' % msg1
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad', 'normal']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=mward,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' ,
                  'expectedRC'       : 49 }
              </call>
            </iterate>

            <message>
               '%s Preamble Step 4. Admin Resetting User Pwd' % msg1
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'dsAuthzID'              : 'dn:uid=mward,%s' % basedn ,
                    'dsNewPassword'          : 'adminnormal' }
            </call>
            
            <message>
               '%s Step 5. User Bind With New Password' % msg1
            </message>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=mward,%s' % basedn ,
                  'dsInstancePswd'   : 'adminnormal' ,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' }
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

       <!--- Test Case information
           #@TestMarker         Lockout Duration
           #@TestName           Admin Change Lockout Duration
           #@TestIssue          none
           #@TestPurpose        Admin making Lockout Duration Short
           #@TestPreamble       none
           #@TestStep           set lockout-duration to 5 s
           #@TestPostamble      none
           #@TestResult         Success if the 5 steps are PASS
        -->
        
        <testcase name="getTestCaseName('Make Lockout Duration Short')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Admin Making Lockout Duration Short' % msg
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-duration' ,
                    'attributeValue'         : '10 s' }
            </call>
            
            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

       <!--- Test Case information
           #@TestMarker         Lockout Duration
           #@TestName           Testing Lockout Duration Short
           #@TestIssue          none
           #@TestPurpose        Testing Lockout Duration Short
           #@TestPreamble       none
           #@TestStep           Step 1. User Lockout with Bad Pwd 3x 
                                SearchObject returns 49 (3x with bad, 
                                1x with correct one)
                                Sleeping sleepForMilliSeconds 8000
           #@TestStep           Step 2. User Changing Password returns 0
           #@TestStep           Step 3. Admin Resetting User Pwd
           #@TestStep           Step 4. User Bind With New Password
                                SearchObject returns 0
           #@TestPostamble      none
           #@TestResult         Success if the 4 steps are PASS
        -->        
        
        <testcase name="getTestCaseName('Short Lockout Duration')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Step 1. User Lockout With Bad Pwd 3x' % msg2
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad', 'adminnormal']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=mward,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' ,
                  'expectedRC'       : 49 }
              </call>
            </iterate>

            <message>
               'Security: Lockout Duration: Short Lockout Duration - Sleeping'
            </message>

            <call function="'Sleep'">
              { 'sleepForMilliSeconds' : '8000' }
            </call>
            
            <message>
               '%s Step 2. User Changing Password' % msg2
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : 'uid=mward,%s' % basedn ,
                    'dsInstancePswd'         : 'adminnormal' ,
                    'dsAuthzID'              : 'dn:uid=mward,%s' % basedn ,
                    'dsNewPassword'          : 'newnormal' }
            </call>
            
            <message>
               '%s Step 3. User Bind With New Password' % msg2
            </message>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=mward,%s' % basedn ,
                  'dsInstancePswd'   : 'newnormal' ,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' }
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

     <!--- Test Case information
           #@TestMarker         Lockout Duration
           #@TestName           Admin Change Lockout Duration
           #@TestIssue          none
           #@TestPurpose        Admin making Lockout Duration Long
           #@TestPreamble       none
           #@TestStep           set lockout-duration to 5 days
           #@TestStep           Check manage-account
                                get-seconds-until-password-reset-lockout
           #@TestPostamble      none
           #@TestResult         Success if the step is PASS
        -->
        
        <testcase name="getTestCaseName('Make Lockout Duration Long')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Admin Making Lockout Duration Long' % msg
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-duration' ,
                    'attributeValue'         : '5 d' }
            </call>
            
            <message>
               '%s Check manage-account %s' % (msg,msg3)
            </message>

            <call function="'manageAccountWithScript'">
               { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                 'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                 'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                 'subcommand'       : msg3 ,
                 'targetDn'         : 'uid=scarter,%s' % basedn  }
            </call>
          
            <script> 
               returnString = STAXResult[0][1]
            </script>
            
          <call function="'checktestString'">
              { 'returnString'       : returnString ,
                'expectedString'     : 'Seconds Until Password Reset Lockout:' }
          </call>
          
            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

 
        <!--- Test Case information
           #@TestMarker         Lockout Duration
           #@TestName           Testing Lockout Duration Long
           #@TestIssue          none
           #@TestPurpose        Testing Lockout Duration Long
           #@TestPreamble       none
           #@TestStep           Step 1. User Initial Bind returns 0
           #@TestStep           Step 2. User Lockout with Bad Pwd 3x 
                                SearchObject returns 49 (3x with bad, 
                                1x with correct one)
                                Sleeping sleepForMilliSeconds 8000
           #@TestStep           Step 3. User Changing Password returns 49
           #@TestStep           Step 4. User Bind With New Password
                                SearchObject returns 49
           #@TestPostamble      none
           #@TestResult         Success if the 4 steps are PASS
        -->
        
        <testcase name="getTestCaseName('Long Lockout Duration')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Long Lockout Duration Step 1. User Initial Bind' % msg
            </message>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=bjablons,%s' % basedn ,
                  'dsInstancePswd'   : 'strawberry' ,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' }
            </call>
    
            <message>
               '%s Step 2. User Lockout With Bad Pwd 3x' % msg4
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad', 'strawberry']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=bjablons,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' ,
                  'expectedRC'       : 49 }
              </call>
            </iterate>

            <message>
               'Security: Lockout Duration: Long Lockout Duration - Sleeping'
            </message>

            <call function="'Sleep'">
              { 'sleepForMilliSeconds' : '8000' }
            </call>
            
            <message>
               '%s Step 3. User Changing Password' % msg4
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : 'uid=bjablons,%s' % basedn ,
                    'dsInstancePswd'         : 'strawberry' ,
                    'dsAuthzID'              : 'dn:uid=bjablons,%s' % basedn ,
                    'dsNewPassword'          : 'newberry' ,
                    'expectedRC'             : 49 }
            </call>
            
            <message>
               '%s Step 4. User Bind With New Password' % msg4
            </message>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=bjablons,%s' % basedn ,
                  'dsInstancePswd'   : 'newberry' ,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base',
                  'expectedRC'       : 49 } 
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>


        <!--- Test Case information
           #@TestMarker         Lockout Duration
           #@TestName           Postamble
           #@TestIssue          none
           #@TestPurpose        Postamble
           #@TestPreamble       none
           #@TestStep           Step 1. Admin resetting Lockout Duration to 0
           #@TestStep           Step 2. User Lockout with Bad Pwd 3x 
                                SearchObject returns 49 (3x with bad, 
                                1x with correct one)
                                Sleeping sleepForMilliSeconds 8000
           #@TestStep           Step 3. User Changing Password returns 0
           #@TestStep           Step 4. User Bind With New Password
                                SearchObject returns 0
           #@TestPostamble      none
           #@TestResult         Success if the 4 steps are PASS
        -->
        
        <testcase name="getTestCaseName('Postamble')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               '%s Postamble Step 1. Admin Resetting Lockout Duration' % msg
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-duration' ,
                    'attributeValue'         : '0 s' }
            </call>
            
            <message>
               '%s Postamble Step 2. User Lockout With Bad Pwd 3x' % msg
            </message>

            <script>
                search_pwds = ['bad', 'bad', 'bad', 'newnormal']
            </script>
            
            <iterate var="pwds" in="search_pwds" indexvar="index">
              <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=mward,%s' % basedn ,
                  'dsInstancePswd'   : '%s' % pwds,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' ,
                  'expectedRC'       : 49 }
              </call>
            </iterate>

            <message>
               '%s Postamble Step 3. Admin Resetting User Pwd' % msg
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'dsAuthzID'              : 'dn:uid=mward,%s' % basedn ,
                    'dsNewPassword'          : 'normal' }
            </call>
            
            <message>
               '%s Postamble Step 4. User Bind With New Password' % msg
            </message>

            <call function="'SearchObject'">
                { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                  'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                  'dsInstanceDn'     : 'uid=mward,%s' % basedn ,
                  'dsInstancePswd'   : 'normal' ,
                  'dsBaseDN'         : 'dc=example,dc=com' ,
                  'dsFilter'         : 'objectclass=*'  ,
                  'extraParams'      : '-s base' }
            </call>

            <message>
               '%s Postamble Step 5. Admin Resetting Lockout Count' % msg
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'lockout-failure-count' ,
                    'attributeValue'         : '0' }
            </call>
            
            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>

     </sequence>

  </function>

</stax>
