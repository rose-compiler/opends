<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE stax SYSTEM "../../../../shared/stax.dtd">
<!--
 ! CDDL HEADER START
 !
 ! The contents of this file are subject to the terms of the
 ! Common Development and Distribution License, Version 1.0 only
 ! (the "License").  You may not use this file except in compliance
 ! with the License.
 !
 ! You can obtain a copy of the license at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE
 ! or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 ! See the License for the specific language governing permissions
 ! and limitations under the License.
 !
 ! When distributing Covered Code, include this CDDL HEADER in each
 ! file and include the License file at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 ! add the following below this CDDL HEADER, with the fields enclosed
 ! by brackets "[]" replaced with your own identifying information:
 !      Portions Copyright [yyyy] [name of copyright owner]
 !
 ! CDDL HEADER END
 !
 !      Copyright 2006-2008 Sun Microsystems, Inc.
 ! -->
<stax>

  <defaultcall function="user_pwd_policy"/>

  <function name="user_pwd_policy">

      <sequence>
        
        <!--- Test Suite information
         #@TestSuiteName       User Password Policy
         #@TestSuitePurpose    User Password Policy
         #@TestSuiteGroup      User Password Policy
         #@TestScript          security_user_pwd_policy.xml
        -->

        <!--- Define default value for basedn -->
         <script>
     basedn = 'ou=people,ou=password tests,o=Pwd Policy Tests,dc=example,dc=com'
     basepwp = 'cn=Default Password Policy,cn=Password Policies,cn=config'
     msg = 'Security: Pwd Policy:'
        </script>       
        
        <!--- Test Case information
          #@TestMarker         User Password Policy
          #@TestName           Check ds-cfg-allow-user-password-changes
          #@TestIssue          none
          #@TestPurpose        Checking existence of 
                               ds-cfg-allow-user-password-changes behavior tests
          #@TestPreamble       none
          #@TestStep           Check ds-cfg-allow-user-password-changes
          #@TestStep           Check the output of the command.
          #@TestPostamble      none
          #@TestResult         Success if compareEntry returns 0 and the 
                               output is correct.
        -->
        <testcase name="getTestCaseName('Preamble')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
             'Security: Pwd Policy: Check of ds-cfg-allow-user-password-changes'
            </message>

            <call function="'compareEntry'">
            { 'dsInstanceHost'      : DIRECTORY_INSTANCE_HOST ,
              'dsInstancePort'      : DIRECTORY_INSTANCE_PORT ,
              'dsInstanceDn'        : DIRECTORY_INSTANCE_DN ,
              'dsInstancePswd'      : DIRECTORY_INSTANCE_PSWD ,
              'attrToBeCompared'    : 'ds-cfg-allow-user-password-changes:true',
              'entryToBeCompared'   : basepwp }
            </call>

            <call function="'testCase_Postamble'"/>

          </sequence>
        </testcase>


        <!--- Test Case information
          #@TestMarker          User Password Policy
          #@TestName            Check User Search With Password
          #@TestIssue           none
          #@TestPurpose         Check User Search With Password
          #@TestPreamble        none
          #@TestStep            Check User Search With Password
          #@TestStep            Check the output of the command.
          #@TestPostamble       none
          #@TestResult          Success if SearchObject returns 0
        -->

        <testcase name="getTestCaseName('User Search')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Pwd Policy: User Searching With Password'
            </message>

            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'   : 'sprain' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*'  ,
                'extraParams'      : '-s base' }
            </call>

            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
          #@TestMarker          User Password Policy
          #@TestName            Check User Search With Bad Password
          #@TestIssue           none
          #@TestPurpose         Check User Search With Bad Password
          #@TestPreamble        none
          #@TestStep            Check User Search With Bad Password
          #@TestPostamble       none
          #@TestResult          Success if SearchObject returns 49
        -->
        <testcase name="getTestCaseName('User Search With Bad Password')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Pwd Policy: User Searching With Bad Password'
            </message>

            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'   : 'newsprain' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*'  ,
                'extraParams'      : '-s base' ,
                'expectedRC'       : 49 }
            </call>

            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
        #@TestMarker          User Password Policy
        #@TestName            User With Bad Credentials Change Password
        #@TestIssue           none
        #@TestPurpose         Check User Search With Bad Credentials
        #@TestPreamble        none
        #@TestStep            Check User Search With Bad Credentials
        #@TestPostamble       none
        #@TestResult          Success if ldapPasswordModifyWithScript returns 49
        -->
 <testcase name="getTestCaseName('User With Bad Credentials Change Password')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
          'Security: Pwd Policy: User with Bad Credentials Changing Password'
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : 'uid=scarter,%s' % basedn ,
                    'dsInstancePswd'         : 'BadPwd' ,
                    'dsAuthzID'              : 'dn:uid=scarter,%s' % basedn ,
                    'dsNewPassword'          : 'newsprain' ,
                    'expectedRC'             : 49  }
            </call>
            
            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
          #@TestMarker          User Password Policy
          #@TestName            User Change Password
          #@TestIssue           none
          #@TestPurpose         User Change Password
          #@TestPreamble        none
          #@TestStep            Step 1 User Change Password
          #@TestStep            Step 2 User Searching With Old Password
          #@TestStep            Step 3 User Searching With New Password
          #@TestPostamble       none
          #@TestResult          Success if SearchObject returns 0
        -->
        <testcase name="getTestCaseName('User Change Password')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Pwd Policy: Step 1 User Changing Password'
            </message>

            <call function="'ldapPasswordModifyWithScript'">
              { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'           : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'         : 'sprain' ,
                'dsAuthzID'              : 'dn:uid=scarter,%s' % basedn ,
                'dsNewPassword'          : 'newsprain' ,
               }
            </call>
            
            <message>
               'Security: Pwd Policy: Step 2 User Searching With Old Password'
            </message>

            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'   : 'sprain' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*' ,
                'extraParams'      : '-s base' ,
                'expectedRC'       : 49 }
            </call>

            <message>
               'Security: Pwd Policy: Step 3 User Searching With New Password'
            </message>

            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'   : 'newsprain' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*' ,
                'extraParams'      : '-s base' }
            </call>

            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
        #@TestMarker          User Password Policy
        #@TestName            Admin Change Password Policy
        #@TestIssue           none
        #@TestPurpose         Admin Change Password Policy
        #@TestPreamble        none
        #@TestStep            Step 1 Modify the Password Policy by setting
                              allow-user-password-changes to false
        #@TestStep            Step 2 User resetting password should fail (49)
        #@TestStep            Step 3 Check manage-account get-password-is-reset
        #@TestPostamble       none
        #@TestResult          Success if ldapPasswordModifyWithScript returns 49
        -->
        
        <testcase name="getTestCaseName('Admin Change Password Policy')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
               'Security: Pwd Policy: Step 1 Admin Changing Password Policy'
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'allow-user-password-changes' ,
                    'attributeValue'         : 'false' }
            </call>
            
            <message>
               'Security: Pwd Policy: Step 2 User Resetting Password'
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : 'uid=scarter,%s' % basedn ,
                    'dsInstancePswd'         : 'sprain' ,
                    'dsAuthzID'              : 'dn:uid=scarter,%s' % basedn ,
                    'dsNewPassword'          : 'sprain' ,
                    'expectedRC'             : 49 }
            </call>
            
            <message>
               '%s Step 3 Check manage-account get-password-is-reset' % msg
            </message>

          <call function="'manageAccountWithScript'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'get-password-is-reset' ,
                'targetDn'         : 'uid=scarter,%s' % basedn  }
          </call>
          
          <script> 
             returnString = STAXResult[0][1]
          </script>
            
          <call function="'checktestString'">
              { 'returnString'       : returnString ,
                'expectedString'     : 'Password Is Reset:  false' }
          </call>
            
            <call function="'testCase_Postamble'"/>
            
          </sequence>
        </testcase>

        <!--- Test Case information
          #@TestMarker          User Password Policy
          #@TestName            Admin Reset Password Policy
          #@TestIssue           none
          #@TestPurpose         Admin Reset Password Policy
          #@TestPreamble        none
          #@TestStep            Step 1 - Admin Resetting Password Policy
                                set allow-user-password-changes to true
          #@TestStep            Step 2 - User Resetting Password
          #@TestStep            Step 3 - Check manage-account 
                                get-password-is-reset
          #@TestStep            Step 4 - check manage-account
                                get-password-changed-time
          #@TestStep            Step 5 - User Searching With Old Password
          #@TestStep            Step 6 - User Searching With New Password
          #@TestPostamble       none
          #@TestResult          Success if SearchObject returns 0.
        -->
        <testcase name="getTestCaseName('Postamble')">
          <sequence>
            <call function="'testCase_Preamble'"/>
            <message>
     'Security: Pwd Policy: Postamble Step 1 - Admin Resetting Password Policy'
            </message>

            <call function="'modifyPwdPolicy'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'           : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'         : DIRECTORY_INSTANCE_PSWD ,
                    'propertyName'           : 'Default Password Policy' ,
                    'attributeName'          : 'allow-user-password-changes' ,
                    'attributeValue'         : 'true' }
            </call>
            
            <message>
             'Security: Pwd Policy: Postamble Step 2 - User Resetting Password'
            </message>

            <call function="'ldapPasswordModifyWithScript'">
                  { 'dsInstanceHost'         : DIRECTORY_INSTANCE_HOST ,
                    'dsInstancePort'         : DIRECTORY_INSTANCE_PORT ,
                    'dsInstanceDn'           : 'uid=scarter,%s' % basedn ,
                    'dsInstancePswd'         : 'newsprain' ,
                    'dsAuthzID'              : 'dn:uid=scarter,%s' % basedn ,
                    'dsNewPassword'          : 'sprain' ,
                  }
            </call>
            
            <message>
               '%s Step 3 Check manage-account get-password-is-reset' % msg
            </message>

            <call function="'manageAccountWithScript'">
                  { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                    'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                    'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                    'subcommand'       : 'get-password-is-reset' ,
                    'targetDn'         : 'uid=scarter,%s' % basedn  }
            </call>
          
            <script> 
               returnString = STAXResult[0][1]
            </script>
            
            <call function="'checktestString'">
               { 'returnString'       : returnString ,
                 'expectedString'     : 'Password Is Reset:  false' }
            </call>
            
            <message>
               '%s Step 4 Check manage-account get-password-changed-time' % msg
            </message>

            <call function="'manageAccountWithScript'">
               { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                 'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                 'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                 'subcommand'       : 'get-password-changed-time' ,
                 'targetDn'         : 'uid=scarter,%s' % basedn  }
            </call>
          
          <script> 
             returnString = STAXResult[0][1]
          </script>
            
          <call function="'checktestString'">
              { 'returnString'       : returnString ,
                'expectedString'     : 'Password Changed Time:  2' }
          </call>
          
            <message>
               '%s Postamble Step 5 - User Searching With Old Password' % msg
            </message>

            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'   : 'newsprain' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*' ,
                'extraParams'      : '-s base' ,
                'expectedRC'       : 49 }
            </call>

            <message>
               '%s Postamble Step 6 - User Searching With New Password' % msg
            </message>

            <call function="'SearchObject'">
              { 'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstancePort'   : DIRECTORY_INSTANCE_PORT ,
                'dsInstanceDn'     : 'uid=scarter,%s' % basedn ,
                'dsInstancePswd'   : 'sprain' ,
                'dsBaseDN'         : 'dc=example,dc=com' ,
                'dsFilter'         : 'objectclass=*' ,
                'extraParams'      : '-s base' }
            </call>

            <call function="'testCase_Postamble'"/>
              
          </sequence>
        </testcase>

      </sequence>

  </function>

</stax>
