<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE stax SYSTEM "../../../shared/stax.dtd">
<!--
 ! CDDL HEADER START
 !
 ! The contents of this file are subject to the terms of the
 ! Common Development and Distribution License, Version 1.0 only
 ! (the "License").  You may not use this file except in compliance
 ! with the License.
 !
 ! You can obtain a copy of the license at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE
 ! or https://OpenDS.dev.java.net/OpenDS.LICENSE.
 ! See the License for the specific language governing permissions
 ! and limitations under the License.
 !
 ! When distributing Covered Code, include this CDDL HEADER in each
 ! file and include the License file at
 ! trunk/opends/resource/legal-notices/OpenDS.LICENSE.  If applicable,
 ! add the following below this CDDL HEADER, with the fields enclosed
 ! by brackets "[]" replaced with your own identifying information:
 !      Portions Copyright [yyyy] [name of copyright owner]
 !
 ! CDDL HEADER END
 !
 !      Copyright 2008-2010 Sun Microsystems, Inc.
 ! -->
<stax>

  <defaultcall function="snmp_trap_customconf"/>

  <function name="snmp_trap_customconf">

    <sequence>

      <block name="'snmp_trap_customconf'">

        <sequence>

          <!--- Test Suite information
            #@TestSuiteName       SNMP trap with custom configuration
            #@TestSuitePurpose    Check SNMP trap with custom configuration.
            #@TestSuiteGroup      SNMP trap with custom configuration
            #@TestScript          snmp_trap_customconf.xml
          -->
          <script>
            if not CurrentTestPath.has_key('group'):
              CurrentTestPath['group'] = 'snmp'
            CurrentTestPath['suite'] = STAXCurrentBlock
          </script>

          <call function="'testSuite_Preamble'"/>

          <!--- Define default values -->
          <script>
            defTrapsCommunityProp = '%s' % SNMP_PROPERTIES['traps-community']
            newTrapsCommunityProp = 'myCommunity'
            newTrapsDestProp = DIRECTORY_INSTANCE_HOST
            wrongTrapsDestProp = 'wrongtrapdestination'
            outputFile2 = '%s/timer.out' % DIRECTORY_INSTANCE_DIR
            timerDuration2 = '5m'
            serverLogFile = '%s/%s/logs/server.out' \
              % (DIRECTORY_INSTANCE_DIR, OPENDSNAME)
          </script>

          <!--- Test Case information
            #@TestMarker          SNMP trap with custom configuration
            #@TestName            trap_customconf: TODO
            #@TestIssue           3435
            #@TestPurpose         TODO.
            #@TestPreamble        none
            #@TestStep            TODO.
            #@TestPostamble       none
            #@TestResult          TODO.
          -->
          <testcase name="getTestCaseName
          ('trap_customconf: change the community property')">

            <sequence>

              <call function="'testCase_Preamble'"/>

              <message>
                'trap_customconf: change the community property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--set traps-community:%s' \
                                     % newTrapsCommunityProp
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <timer duration="timerDuration2">
                <sequence>
                  <parallel>
                    <sequence>
                      <message>
                        'trap_customconf: run the trap listener'
                      </message>

                      <call function="'SNMPTrapListener'">
                        {
                        'location'          : STAF_REMOTE_HOSTNAME ,
                        'snmpHost'          : DIRECTORY_INSTANCE_HOST ,
                        'snmpTrapPort'      : SNMP_TRAP_PORT ,
                        'snmpTrapCommunity' : defTrapsCommunityProp ,
                        'snmpTrapNumber'    : '0,2,0,0,0,0' ,
                        'outputFile'        : outputFile2 ,
                        'knownIssue'        : '3435'
                        }
                      </call>
                    </sequence>
                    <sequence>
                      <message>
                        'trap_customconf: wait event from the trap listener'
                      </message>

                      <call function="'WaitEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_customconf: delete event from the trap listener'
                      </message>
                      <call function="'DeleteEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_defaultconf: restart the server'
                      </message>
                      <call function="'StopDsWithScript'">
                        { 'location'  : STAF_REMOTE_HOSTNAME }
                      </call>
                      <call function="'StartDsWithScript'">
                        { 'location'  : STAF_REMOTE_HOSTNAME }
                      </call>
                      <!--- Check that DS started -->
                      <call function="'isAlive'">
                        {
                        'noOfLoops'        : 10 ,
                        'noOfMilliSeconds' : 2000
                        }
                      </call>

                      <message>
                        'trap_customconf: send event to the trap listener'
                      </message>

                      <call function="'SendEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SendTrap/Complete'
                        }
                      </call>
                    </sequence>
                  </parallel>
                </sequence>
              </timer>

              <script>timerRC2 = RC</script>

              <call function="'checkTimerResult'">
                {
                'location'      : STAF_REMOTE_HOSTNAME ,
                'outputFile'    : outputFile2 ,
                'timerDuration' : timerDuration2 ,
                'timerRC'       : timerRC2
                }
              </call>

              <timer duration="timerDuration2">
                <sequence>
                  <parallel>
                    <sequence>
                      <message>
                        'trap_customconf: run the trap listener'
                      </message>

                      <call function="'SNMPTrapListener'">
                        {
                        'location'          : STAF_REMOTE_HOSTNAME ,
                        'snmpHost'          : DIRECTORY_INSTANCE_HOST ,
                        'snmpTrapPort'      : SNMP_TRAP_PORT ,
                        'snmpTrapCommunity' : newTrapsCommunityProp ,
                        'snmpTrapNumber'    : '2,0,0,0,0,0' ,
                        'outputFile'        : outputFile2 ,
                        'knownIssue'       : '3435'
                        }
                      </call>
                    </sequence>
                    <sequence>
                      <message>
                        'trap_customconf: wait event from the trap listener'
                      </message>

                      <call function="'WaitEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_customconf: delete event from the trap listener'
                      </message>
                      <call function="'DeleteEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_defaultconf: restart the server'
                      </message>
                      <call function="'StopDsWithScript'">
                        { 'location'  : STAF_REMOTE_HOSTNAME }
                      </call>
                      <call function="'StartDsWithScript'">
                        { 'location'  : STAF_REMOTE_HOSTNAME }
                      </call>
                      <!--- Check that DS started -->
                      <call function="'isAlive'">
                        {
                        'noOfLoops'        : 10 ,
                        'noOfMilliSeconds' : 2000
                        }
                      </call>

                      <message>
                        'trap_customconf: send event to the trap listener'
                      </message>

                      <call function="'SendEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SendTrap/Complete'
                        }
                      </call>
                    </sequence>
                  </parallel>
                </sequence>
              </timer>

              <script>timerRC2 = RC</script>

              <call function="'checkTimerResult'">
                {
                'location'      : STAF_REMOTE_HOSTNAME ,
                'outputFile'    : outputFile2 ,
                'timerDuration' : timerDuration2 ,
                'timerRC'       : timerRC2
                }
              </call>

              <message>
                'trap_customconf: restore the traps-community property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--set traps-community:%s' \
                                     % defTrapsCommunityProp
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>
              
              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>

          <!--- Test Case information
            #@TestMarker          SNMP trap with custom configuration
            #@TestName            trap_customconf: TODO
            #@TestIssue           3435
            #@TestPurpose         TODO.
            #@TestPreamble        none
            #@TestStep            TODO.
            #@TestPostamble       none
            #@TestResult          TODO.
          -->
          <testcase name="getTestCaseName
          ('trap_customconf: set traps-destination property to correct value')">

            <sequence>

              <call function="'testCase_Preamble'"/>

              <message>
                'trap_customconf: change the traps-destination property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--set traps-destination:%s' \
                                     % newTrapsDestProp
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <timer duration="timerDuration2">
                <sequence>
                  <parallel>
                    <sequence>
                      <message>
                        'trap_customconf: run the trap listener'
                      </message>

                      <call function="'SNMPTrapListener'">
                        {
                        'location'          : STAF_REMOTE_HOSTNAME ,
                        'snmpHost'          : DIRECTORY_INSTANCE_HOST ,
                        'snmpTrapPort'      : SNMP_TRAP_PORT ,
                        'snmpTrapCommunity' : defTrapsCommunityProp ,
                        'snmpTrapNumber'    : '2,0,0,0,0,0' ,
                        'outputFile'        : outputFile2 ,
                        'knownIssue'        : '3435'
                        }
                      </call>
                    </sequence>
                    <sequence>
                      <message>
                        'trap_customconf: wait event from the trap listener'
                      </message>

                      <call function="'WaitEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_customconf: delete event from the trap listener'
                      </message>
                      <call function="'DeleteEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_defaultconf: restart the server'
                      </message>
                      <call function="'StopDsWithScript'">
                        { 'location'  : STAF_REMOTE_HOSTNAME }
                      </call>
                      <call function="'StartDsWithScript'">
                        { 'location'  : STAF_REMOTE_HOSTNAME }
                      </call>
                      <!--- Check that DS started -->
                      <call function="'isAlive'">
                        {
                        'noOfLoops'        : 10 ,
                        'noOfMilliSeconds' : 2000
                        }
                      </call>

                      <message>
                        'trap_customconf: send event to the trap listener'
                      </message>

                      <call function="'SendEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SendTrap/Complete'
                        }
                      </call>
                    </sequence>
                  </parallel>
                </sequence>
              </timer>

              <script>timerRC2 = RC</script>

              <call function="'checkTimerResult'">
                {
                'location'      : STAF_REMOTE_HOSTNAME ,
                'outputFile'    : outputFile2 ,
                'timerDuration' : timerDuration2 ,
                'timerRC'       : timerRC2
                }
              </call>

              <message>
                'trap_customconf: restore the traps-destination property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--reset traps-destination'
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>

          <!--- Test Case information
            #@TestMarker          SNMP trap with custom configuration
            #@TestName            trap_customconf: TODO
            #@TestIssue           none
            #@TestPurpose         TODO.
            #@TestPreamble        none
            #@TestStep            TODO.
            #@TestPostamble       none
            #@TestResult          TODO.
          -->
          <testcase name="getTestCaseName
          ('trap_customconf: set traps-destination property to a wrong value')">

            <sequence>

              <call function="'testCase_Preamble'"/>

              <message>
                'trap_customconf: change the traps-destination property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--set traps-destination:%s' \
                                     % wrongTrapsDestProp
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <timer duration="timerDuration2">
                <sequence>
                  <parallel>
                    <sequence>
                      <message>
                        'trap_customconf: run the trap listener'
                      </message>

                      <call function="'SNMPTrapListener'">
                        {
                        'location'          : STAF_REMOTE_HOSTNAME ,
                        'snmpHost'          : DIRECTORY_INSTANCE_HOST ,
                        'snmpTrapPort'      : SNMP_TRAP_PORT ,
                        'snmpTrapCommunity' : defTrapsCommunityProp ,
                        'snmpTrapNumber'    : '0,0,0,0,0,0' ,
                        'outputFile'        : outputFile2
                        }
                      </call>
                    </sequence>
                    <sequence>
                      <message>
                        'trap_customconf: wait event from the trap listener'
                      </message>

                      <call function="'WaitEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_customconf: delete event from the trap listener'
                      </message>
                      <call function="'DeleteEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_defaultconf: restart the server'
                      </message>
                      <call function="'StopDsWithScript'">
                        {
                        'location'    : STAF_REMOTE_HOSTNAME ,
                        'dsHost'      : DIRECTORY_INSTANCE_HOST ,
                        'dsAdminPort' : DIRECTORY_INSTANCE_ADMIN_PORT ,
                        'dsBindDN'    : DIRECTORY_INSTANCE_DN ,
                        'dsBindPwd'   : DIRECTORY_INSTANCE_PSWD
                        }
                      </call>
                      <call function="'StartDsWithScript'">
                        { 'location' : STAF_REMOTE_HOSTNAME }
                      </call>
                      <!--- Check that DS started -->
                      <call function="'isAlive'">
                        {
                        'noOfLoops'        : 10 ,
                        'noOfMilliSeconds' : 2000
                        }
                      </call>
                      
                      <script>
                        msg1 = 'Traps Destination %s is an unknown host.' \
                               % wrongTrapsDestProp
                        msg2 = 'Traps will not be sent to this destination'
                        msg = '%s %s' % (msg1, msg2)
                      </script>

                      <call function="'grep'">
                        {
                        'location'  : STAF_REMOTE_HOSTNAME ,
                        'filename'  : serverLogFile ,
                        'testString': msg
                        }
                      </call>
                      
                      <script>
                        msg1 = 'No valid trap destinations has been found.'
                        msg2 = 'No trap will be sent'
                        msg = '%s %s' % (msg1, msg2)
                      </script>
                      
                      <call function="'grep'">
                        {
                        'location'  : STAF_REMOTE_HOSTNAME ,
                        'filename'  : serverLogFile ,
                        'testString': msg
                        }
                      </call>
                      
                      <message>
                        'trap_customconf: send event to the trap listener'
                      </message>

                      <call function="'SendEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SendTrap/Complete'
                        }
                      </call>
                    </sequence>
                  </parallel>
                </sequence>
              </timer>

              <script>timerRC2 = RC</script>

              <call function="'checkTimerResult'">
                {
                'location'      : STAF_REMOTE_HOSTNAME ,
                'outputFile'    : outputFile2 ,
                'timerDuration' : timerDuration2 ,
                'timerRC'       : timerRC2
                }
              </call>

              <message>
                'trap_customconf: restore the traps-destination property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--reset traps-destination'
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>

          <!--- Test Case information
            #@TestMarker          SNMP trap with custom configuration
            #@TestName            trap_customconf: TODO
            #@TestIssue           3435
            #@TestPurpose         TODO.
            #@TestPreamble        none
            #@TestStep            TODO.
            #@TestPostamble       none
            #@TestResult          TODO.
          -->
          <testcase name="getTestCaseName
          ('trap_customconf: set traps-destination property to a list')">

            <sequence>

              <call function="'testCase_Preamble'"/>

              <message>
                'trap_customconf: set traps-destination property to a list'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--set traps-destination:%s' \
                                     % wrongTrapsDestProp
                }
              </call>
              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--add traps-destination:%s' \
                                     % newTrapsDestProp
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <timer duration="timerDuration2">
                <sequence>
                  <parallel>
                    <sequence>
                      <message>
                        'trap_customconf: run the trap listener'
                      </message>

                      <call function="'SNMPTrapListener'">
                        {
                        'location'          : STAF_REMOTE_HOSTNAME ,
                        'snmpHost'          : DIRECTORY_INSTANCE_HOST ,
                        'snmpTrapPort'      : SNMP_TRAP_PORT ,
                        'snmpTrapCommunity' : defTrapsCommunityProp ,
                        'snmpTrapNumber'    : '2,0,0,0,0,0' ,
                        'outputFile'        : outputFile2 ,
                        'knownIssue'        : '3435'
                        }
                      </call>
                    </sequence>
                    <sequence>
                      <message>
                        'trap_customconf: wait event from the trap listener'
                      </message>

                      <call function="'WaitEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_customconf: delete event from the trap listener'
                      </message>
                      <call function="'DeleteEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SNMPTrapListener/Ready'
                        }
                      </call>

                      <message>
                        'trap_defaultconf: restart the server'
                      </message>
                      <call function="'StopDsWithScript'">
                        {
                        'location'    : STAF_REMOTE_HOSTNAME ,
                        'dsHost'      : DIRECTORY_INSTANCE_HOST ,
                        'dsAdminPort' : DIRECTORY_INSTANCE_ADMIN_PORT ,
                        'dsBindDN'    : DIRECTORY_INSTANCE_DN ,
                        'dsBindPwd'   : DIRECTORY_INSTANCE_PSWD
                        }
                      </call>
                      <call function="'StartDsWithScript'">
                        { 'location' : STAF_REMOTE_HOSTNAME }
                      </call>
                      <!--- Check that DS started -->
                      <call function="'isAlive'">
                        {
                        'noOfLoops'        : 10 ,
                        'noOfMilliSeconds' : 2000
                        }
                      </call>
                      
                      <script>
                        msg1 = 'Traps Destination %s is an unknown host.' \
                               % wrongTrapsDestProp
                        msg2 = 'Traps will not be sent to this destination'
                        msg = '%s %s' % (msg1, msg2)
                      </script>
                      
                      <call function="'grep'">
                        {
                        'location'  : STAF_REMOTE_HOSTNAME ,
                        'filename'  : serverLogFile ,
                        'testString': msg
                        }
                      </call>

                      <message>
                        'trap_customconf: send event to the trap listener'
                      </message>

                      <call function="'SendEvent'">
                        {
                        'location' : STAF_REMOTE_HOSTNAME ,
                        'name'     : 'SendTrap/Complete'
                        }
                      </call>
                    </sequence>
                  </parallel>
                </sequence>
              </timer>

              <script>timerRC2 = RC</script>

              <call function="'checkTimerResult'">
                {
                'location'      : STAF_REMOTE_HOSTNAME ,
                'outputFile'    : outputFile2 ,
                'timerDuration' : timerDuration2 ,
                'timerRC'       : timerRC2
                }
              </call>

              <message>
                'trap_customconf: restore the traps-destination property'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--reset traps-destination'
                }
              </call>

              <message>
                'trap_customconf: restart the SNMP Connection Handler'
              </message>

              <call function="'restartSNMPConnectionHandler'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>

          <!--- Test Case information
            #@TestMarker          SNMP trap with custom configuration
            #@TestName            trap_customconf: TODO
            #@TestIssue           none
            #@TestPurpose         TODO.
            #@TestPreamble        none
            #@TestStep            TODO.
            #@TestPostamble       none
            #@TestResult          TODO.
          -->
          <testcase name="getTestCaseName
          ('trap_customconf: set the traps-community property to a list')">

            <sequence>

              <call function="'testCase_Preamble'"/>

              <message>
                'trap_customconf: set the traps-community property to a list'
              </message>

              <call function="'dsconfig'">
                {
                'dsInstanceHost'   : DIRECTORY_INSTANCE_HOST ,
                'dsInstanceDn'     : DIRECTORY_INSTANCE_DN ,
                'dsInstancePswd'   : DIRECTORY_INSTANCE_PSWD ,
                'subcommand'       : 'set-connection-handler-prop' ,
                'objectType'       : 'handler-name' ,
                'objectName'       : 'SNMP Connection Handler' ,
                'optionsString'    : '--add traps-community:%s' \
                                     % newTrapsCommunityProp ,
                'expectedRC'       : 1
                }
              </call>

              <script>
                returnString = STAXResult[0][1]

                returnString = STAXResult[0][1]
                msg1 = 'It is not possible to specify multiple values for the'
                msg2 = 'SNMP Connection Handler%sproperty "traps-community"' \
                       % newLine
                msg3 = 'as it is single-valued'
                msg = '%s %s %s' % (msg1, msg2, msg3)
              </script>

              <call function="'checktestString'">
                {
                'returnString'   : returnString ,
                'expectedString' : msg
                }
              </call>

              <call function="'testCase_Postamble'"/>

            </sequence>

          </testcase>

          <call function="'testSuite_Postamble'"/>

        </sequence>

      </block>

    </sequence>

  </function>

</stax>
